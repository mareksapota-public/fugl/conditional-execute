// Copyright 2021 Marek Sapota

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

package org.sapota.marek.fugl.conditional_execute.bin

import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.parameters.arguments.argument
import com.github.ajalt.clikt.parameters.arguments.multiple
import com.github.ajalt.clikt.parameters.groups.OptionGroup
import com.github.ajalt.clikt.parameters.groups.cooccurring
import com.github.ajalt.clikt.parameters.options.convert
import com.github.ajalt.clikt.parameters.options.flag
import com.github.ajalt.clikt.parameters.options.option
import com.github.ajalt.clikt.parameters.options.required
import com.github.ajalt.clikt.parameters.types.long
import kotlinx.coroutines.runBlocking
import org.sapota.marek.fugl.conditional_execute.PeriodicExecutor
import org.sapota.marek.fugl.conditional_execute.PowerSupply
import org.sapota.marek.readable_time_parser.DurationParser
import org.sapota.marek.shell_command.ShellCommand
import org.sapota.marek.shell_command.ShellExecutor
import org.sapota.marek.shell_command.exceptions.LockFailedException
import org.sapota.marek.shell_command.lock.withLock
import java.io.File
import kotlin.time.Duration

class ExclusiveLockOption : OptionGroup() {
    val lockFile by option(
        "--lock-file",
        help = "Path to the lock file",
    )
        .convert("PATH") { File(it) }
        .required()

    val timestampFile by option(
        "--timestamp-file",
        help = "Path to timestamp file, defaults to <lock file path>.timestamp",
    ).convert("PATH") { File(it) }

    val wait by option(
        "--lock-wait",
        help = "Exit/wait on locking errors, defaults to exit",
    ).flag(
        "--lock-exit",
        default = false,
    )

    val executionInterval: Duration? by option(
        "--execution-interval",
        help = "Time between executions, based on lockfile mtime, in seconds " +
            "or add a time unit",
    )
        .convert("DURATION") {
            DurationParser.parse(it)!!
        }
}

class ConditionalExecuteBin : CliktCommand() {
    val timeout: Long? by option(
        "--timeout",
        "-t",
        help = "Timeout for the command, in seconds",
    )
        .long()

    val cwd: File? by option(
        "--cwd",
        "-C",
        help = "Directory to execute the command in",
    )
        .convert("PATH") { File(it) }

    val onBattery by option(
        "--on-battery",
        help = "Allow running on battery power",
    ).flag(
        "--ac-only",
        default = true,
    )

    val exclusiveLock: ExclusiveLockOption? by ExclusiveLockOption()
        .cooccurring()

    val command: String by argument(help = "Command to run")

    val arguments: List<String> by argument(
        help = "Command arguments, use -- to pass arguments to the command " +
            "instead of this binary",
    ).multiple()

    suspend fun onBatteryBlock(codeBlock: suspend () -> Unit) {
        if (onBattery || PowerSupply.online) {
            codeBlock()
        }
    }

    override fun run() = runBlocking {
        val shouldWait = exclusiveLock?.wait ?: false
        val executor = ShellExecutor(timeout = timeout, cwd = cwd)
        val shellCommand = ShellCommand(command)
        shellCommand.addAll(arguments)
        val timestampFile = when (
            val timestampFile = exclusiveLock?.timestampFile
        ) {
            null -> when (val lockFile = exclusiveLock?.lockFile) {
                null -> null
                else -> File("${lockFile.absolutePath}.timestamp")
            }
            else -> timestampFile
        }
        val periodicBlock = suspend {
            PeriodicExecutor(
                timestampFile,
                exclusiveLock?.executionInterval,
                shouldWait,
            ).run {
                onBatteryBlock {
                    executor.run(shellCommand)
                }
            }
        }
        val lockFileBlock = suspend {
            when (val lockFile = exclusiveLock?.lockFile) {
                null -> periodicBlock()
                else -> try {
                    lockFile.withLock(shouldWait) {
                        periodicBlock()
                    }
                } catch (_: LockFailedException) {
                }
            }
        }
        onBatteryBlock {
            lockFileBlock()
        }
    }
}

fun main(args: Array<String>) = ConditionalExecuteBin().main(args)
