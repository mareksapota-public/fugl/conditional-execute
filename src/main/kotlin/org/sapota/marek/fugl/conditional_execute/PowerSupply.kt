// Copyright 2021 Marek Sapota

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

package org.sapota.marek.fugl.conditional_execute

import java.io.File

class PowerSupply private constructor(val id: Int) {
    val online: Boolean
        get() {
            return File(
                "${PowerSupply.SYS_PATH}/AC$id/online",
            ).readText().trim() == "1"
        }

    companion object {
        private const val SYS_PATH = "/sys/class/power_supply"

        val online: Boolean
            get() {
                return PowerSupply.getAll().all { it.online }
            }

        fun getAll(): List<PowerSupply> {
            return File(SYS_PATH)
                .listFiles()
                .filter { Regex("AC[0-9]+").matches(it.name) }
                .map { PowerSupply(it.name.drop(2).toInt()) }
        }
    }
}
