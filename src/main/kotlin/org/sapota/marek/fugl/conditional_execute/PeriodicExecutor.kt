// Copyright 2021 Marek Sapota

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

package org.sapota.marek.fugl.conditional_execute

import kotlinx.coroutines.delay
import java.io.File
import kotlin.time.Duration
import kotlin.time.DurationUnit
import kotlin.time.toDuration

class PeriodicExecutor(
    val timestampFile: File?,
    val executionInterval: Duration?,
    val wait: Boolean,
) {
    companion object {
        val SLEEP_TIME = 10.toDuration(DurationUnit.MILLISECONDS)
    }

    fun shouldRun(): Boolean {
        if (timestampFile == null || executionInterval == null) {
            return true
        }
        if (timestampFile.createNewFile()) {
            // File didn't exist
            return true
        }
        val minRunTime = timestampFile.lastModified() +
            executionInterval.inMilliseconds
        return System.currentTimeMillis() > minRunTime
    }

    fun touchTimestamp() {
        timestampFile?.setLastModified(System.currentTimeMillis())
    }

    suspend fun wait() {
        delay(PeriodicExecutor.SLEEP_TIME)
    }

    tailrec suspend fun run(codeBlock: suspend () -> Unit) {
        when (shouldRun()) {
            true -> {
                touchTimestamp()
                codeBlock()
            }
            false -> {
                if (wait) {
                    wait()
                    run(codeBlock)
                }
            }
        }
    }
}
